package by.action;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsTestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.junit.Test;

public class ActionTest extends StrutsTestCase {

    @Test
    public void testGetActionMapping() {
        ActionMapping mapping = getActionMapping("/test/testAction.action");
        assertNotNull(mapping);
        assertEquals("/test", mapping.getNamespace());
        assertEquals("testAction", mapping.getName());
    }

    @Test
    public void testResultAction() throws Exception {
        request.setParameter("userName", "admin");
        request.setParameter("password", "123");
        request.setParameter("language", "en");

        ActionProxy proxy = getActionProxy("/test/testAction.action");
        assertNotNull(proxy);

        ResultAction action = (ResultAction) proxy.getAction();
        assertNotNull(action);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertEquals("admin", action.getUserName());
    }

}