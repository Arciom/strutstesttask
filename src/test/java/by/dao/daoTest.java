package by.dao;

import by.persistence.User;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DaoTest {

    @Test
    public void userDAO() {

        UserDao userDao = new UserDao();
        User user = userDao.findUserByNamePass("admin", "123");
        assertEquals("admin", user.getUserName());
        assertEquals("123", user.getPassword());
    }
}
