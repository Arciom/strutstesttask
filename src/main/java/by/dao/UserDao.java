package by.dao;

import by.HibernateUtil;
import by.persistence.User;

import org.hibernate.Query;
import org.hibernate.Session;

/**
 * DAO for User entity.
 */
public class UserDao {

	/**
	 *  Finds in th DB user by name & password
	 * @param name Username of the user
	 * @param pass Password of the user
	 * @return User which has such name&pass
     */
	public User findUserByNamePass(String name, String pass) {
		User user = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			String hql = "from User where userName = '" + name +
				         "' and password = '" + pass + "'";
			Query query = session.createQuery(hql);
			user = (User) query.uniqueResult();

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if ((session != null) && (session.isOpen()))
				session.close();
		}
		return user;
	}

}
