package by.action;

import by.dao.UserDao;
import by.persistence.User;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionContext;

import java.util.Locale;

/**
 * Struts2 Action class responds to a user action.
 * Gets result after processing the task login form.
 */
public class ResultAction extends ActionSupport{

    private static final long serialVersionUID = 1L;
	private String userName;
	private String password;
	private String language;

    /**
     * Processing login form.
     */
    public String execute() {

        UserDao userDao = new UserDao();
        User user = userDao.findUserByNamePass(userName, password);
        // choosing the interface language
        if (user !=null) {
            if (!language.equals("")) {
                ActionContext.getContext().setLocale(Locale.forLanguageTag(language));
            } else if (!user.getLanguage().equals("")) {
                ActionContext.getContext().setLocale(Locale.forLanguageTag(user.getLanguage()));
            } else {
                ActionContext.getContext().setLocale(Locale.forLanguageTag("ru"));
            }
            return SUCCESS;

        } else {
            // printing error message when there aren't such user in the DB
            ActionContext.getContext().setLocale(Locale.forLanguageTag(language));
            addFieldError("noSuchUserError", getText("error.noUser"));
            return ERROR;
        }
    }

    /**
     * Validating username & password, they are required.
     */
    @Override
    public void validate() {

        if(getUserName() == null || "".equals(getUserName())) {
            ActionContext.getContext().setLocale(Locale.forLanguageTag(language));
            addActionError(getText("error.username.required"));
        }
        if(getPassword() == null || "".equals(getPassword())) {
            ActionContext.getContext().setLocale(Locale.forLanguageTag(language));
            addActionError(getText("error.password.required"));
        }
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
}