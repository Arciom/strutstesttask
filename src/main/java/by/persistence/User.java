package by.persistence;

import javax.persistence.*;

@Entity
@Table(name="Users")
public class User {

	@Id
	@GeneratedValue
	private Integer id;
	@Column(name = "UserName")
	private String userName;
	@Column(name = "Password")
	private String password;
	@Column(name = "Language")
	private String language;

	public User() {
	}

	public User(String userName, String password, String language) {
		this.userName = userName;
		this.password = password;
		this.language = language;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String toString() {
		return "Users{" +
				"id=" + id +
				", userName='" + userName + '\'' +
				", password='" + password + '\'' +
				", language='" + language + '\'' +
				'}';
	}
} 