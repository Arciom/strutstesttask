<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <title>Welcome page</title>
    <style>
        h3 {
            font-style: italic;
        }

        .container {
            padding: 10px;
            align: center;
        }
    </style>
</head>
<body>

  <div class="container">

  <h2><s:property value="getText('msg.success')" /></h2>

     <h3><s:property value="getText('success.username')" /> <s:property value="userName"/></h3>
     <h3><s:property value="getText('success.password')" /> <s:property value="password"/></h3>
     <h3><s:property value="getText('success.language')" /> <s:property value="language"/></h3>

     <p><a href="login"><s:property value="getText('msg.return')" /></a>.</p>

  </div>

</body>
</html>