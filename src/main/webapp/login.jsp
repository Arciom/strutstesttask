<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <title>Login test task</title>
  <style>
      .tdLabel, tr {
          padding: 6px 9px;
          text-align: left;
      }

      input[type=text], input[type=password], select {
          padding: 6px 10px;
      }

      input[type=submit] {
          background-color: #4CAF50;
          color: white;
          padding: 10px 20px;
          margin: 10px;
          border: none;
          cursor: pointer;
      }

      .container {
          padding: 10px;
          align: center;
      }
  </style>
  <s:head />

</head>
<body>
 <div class="container">
  <h3><s:property value="getText('msg.welcome')" /></h3>

  <s:if test="hasActionErrors()">
	<div class="errorDiv">
		<s:actionerror/>
	</div>
  </s:if>

  <s:form action="result" >

    <s:textfield name="userName" key="label.username" />
    <s:password name="password" key="label.password" />
	<s:select list="{'', 'en','by','ru'}" name="language" key="label.language"></s:select>

    <s:submit key="label.submit.login" align="center" name="submit"
	          method="execute"/>
  </s:form>

  <s:fielderror fieldName="noSuchUserError" />
 </div>

</body>
</html>